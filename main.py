import getopt
import threading
import sys
import logging

from join import *
from socketapi import (
    process_network_info
)
from upload import serve_files
from download import download

unix_options = 'r:sn:p:p:'
gnu_options = ['receive=', 'serve', 'name=', 'path=', 'port=']

JOIN_FILE_PATH = 'peer.p2p'
LISTEN_PORT = 1234

logging.basicConfig(level=logging.DEBUG)


def main(argv):
    try:
        arguments, values = getopt.getopt(argv, unix_options, gnu_options)
    except getopt.error as e:
        print(str(e))
        sys.exit(2)
    serve = False
    receive = False
    serve_file_name = str()
    serve_file_path = str()
    receive_file_name = str()
    for curr_arg, curr_val in arguments:
        if curr_arg == '--serve':
            serve = True
        if curr_arg == '--name':
            serve_file_name = curr_val
        if curr_arg == '--path':
            serve_file_path = curr_val
        if curr_arg == '--receive':
            receive = True
            receive_file_name = curr_val
        if curr_arg == '--port':
            global LISTEN_PORT
            LISTEN_PORT = int(curr_val)
            logging.debug('changing port to {}'.format(curr_val))
    try:
        network_info = join_network('peer.p2p', stay=serve, listen_port=LISTEN_PORT)
    except Exception as e:
        if serve:
            network_info = [(IP, LISTEN_PORT)]
        else:
            logging.info('No network found exiting')
            sys.exit(2)
    logging.debug(LISTEN_PORT)
    process_network_info(network_info)
    if receive:
        download_thread = threading.Thread(target=download, args=(receive_file_name,))
        download_thread.start()
    if serve and serve_file_path and serve_file_name:
        serve_files(LISTEN_PORT, (serve_file_name, serve_file_path))


if __name__ == '__main__':
    main(sys.argv[1:])
